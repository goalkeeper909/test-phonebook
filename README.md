# test-phonebook

Тестовое веб-приложение для создания\хранения\редактирования контактной информации.

Тестовая БД прилагается.
Для запуска выполните команду python manage.py runserver

В случае отсутствия python необходимо последовательно установить python, virtualenv, django при помощи pip.

Более подробно https://djangobook.com/installing-django/