from django.shortcuts import render, redirect
from .models import Member
 
# Create your views here.
 
def index(request):
    members = Member.objects.all()
    context = {'members': members}
    return render(request, 'contacts/index.html', context)
 
def create(request):
    member = Member(name=request.POST['name'], phone=request.POST['phone'], email=request.POST['email'])
    member.save()
    return redirect('/')
 
def edit(request, id):
    members = Member.objects.get(id=id)
    context = {'members': members}
    return render(request, 'contacts/edit.html', context)
 
def update(request, id):
    member = Member.objects.get(id=id)
    member.name = request.POST['name']
    member.phone = request.POST['phone']
    member.email = request.POST['email']
    member.save()
    return redirect('/contacts/')
 
def delete(request, id):
    member = Member.objects.get(id=id)
    member.delete()
    return redirect('/contacts/')
 
def search(request):
    results = Member.objects.filter(name=request.GET.get('name',''))
    context = {'results': results}
    return render(request, 'contacts/search.html', context)