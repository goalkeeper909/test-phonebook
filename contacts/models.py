from django.db import models
 
# Create your models here.
 
class Member(models.Model):
    name = models.CharField(max_length=40)
    phone = models.CharField(max_length=40)
    email = models.EmailField(max_length=40)
 
    def __str__(self):
        return self.name + " " + self.phone + " " + self.email